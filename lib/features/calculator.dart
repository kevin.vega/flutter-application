/* Kevin Vega
Team: Kazuha
Project Name:
Feature: [FEAT-001] Feature title
Feature description: description
 */

import 'package:flutter/material.dart';
import 'package:flutter_application/features/mybutton.dart';
import 'package:math_expressions/math_expressions.dart';

class SecondPage extends StatelessWidget {
  const SecondPage({super.key});

  @override
  Widget build(BuildContext context) {
    //app screen
    return const MaterialApp(
      home: HomePage(),
    );
  }
}

//home page of calculator
class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  HomePageState createState() => HomePageState();
}

class HomePageState extends State<HomePage> {
  var userInput = '';
  var answer = '';

//array of button values
  final List<String> buttons = [
    'C',
    '^',
    '%',
    'DEL',
    '7',
    '8',
    '9',
    '/',
    '4',
    '5',
    '6',
    'x',
    '1',
    '2',
    '3',
    '-',
    '0',
    '.',
    '=',
    '+',
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //header of the calculator app
      appBar: AppBar(
        title: const Text('Calculator'),
      ),
      backgroundColor: Colors.grey,
      body: Column(
        children: <Widget>[
          //input and result widgets
          SizedBox(
            height: (MediaQuery.of(context).size.height) * 0.25,
            width: (MediaQuery.of(context).size.width),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                //user input
                Container(
                  padding: const EdgeInsets.all(20.0),
                  alignment: Alignment.centerRight,
                  child: Text(userInput,
                      style: const TextStyle(
                          fontSize: 18,
                          color: Colors.white,
                          fontWeight: FontWeight.bold)),
                ),
                //answer
                Container(
                  padding: const EdgeInsets.all(15.0),
                  alignment: Alignment.centerRight,
                  child: Text(answer,
                      style: const TextStyle(
                          fontSize: 30,
                          color: Colors.white,
                          fontWeight: FontWeight.bold)),
                ),
              ],
            ),
          ),
          //buttons
          /*
          for the calculator buttons, we've used the color blue, white and orange for the UI
          */
          Expanded(
            child: GridView.builder(
              itemCount: buttons.length,
              gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 4),
              itemBuilder: (BuildContext context, int index) {
                if (index == 0) {
                  //clear button
                  return MyButton(
                    buttonText: buttons[index],
                    buttonTapped: () {
                      setState(() {
                        userInput = '';
                        answer = '0';
                      });
                    },
                    color: Colors.blue,
                    textColor: Colors.black,
                  );
                } else if (index == 1) {
                  //exponent
                  return MyButton(
                    buttonText: buttons[index],
                    color: Colors.blue,
                    textColor: Colors.black,
                    buttonTapped: () {
                      setState(() {
                        userInput += buttons[index];
                      });
                    },
                  );
                } else if (index == 2) {
                  //modulo
                  return MyButton(
                    buttonText: buttons[index],
                    buttonTapped: () {
                      setState(() {
                        userInput += buttons[index];
                      });
                    },
                    color: Colors.blue,
                    textColor: Colors.black,
                  );
                } else if (index == 3) {
                  //delete
                  return MyButton(
                    buttonText: buttons[index],
                    buttonTapped: () {
                      setState(() {
                        userInput =
                            userInput.substring(0, userInput.length - 1);
                      });
                    },
                    color: Colors.blue,
                    textColor: Colors.black,
                  );
                } else if (index == 18) {
                  //equals
                  return MyButton(
                    buttonText: buttons[index],
                    buttonTapped: () {
                      setState(() {});
                      equalPressed();
                    },
                    color: Colors.orange,
                    textColor: Colors.black,
                  );
                } else {
                  return MyButton(
                    //numbers and other operations
                    buttonText: buttons[index],
                    textColor: isOperator(buttons[index])
                        ? Colors.orange
                        : Colors.black,
                    buttonTapped: () {
                      setState(() {
                        userInput += buttons[index];
                      });
                    },
                    color: Colors.white,
                  );
                }
              },
            ),
          ),
        ],
      ),
    );
  }

  void equalPressed() {
    String finalUserInput = userInput;

    finalUserInput = userInput.replaceAll('x', '*');

    Parser p = Parser();
    Expression exp = p.parse(finalUserInput);

    ContextModel cm = ContextModel();

    double eval = exp.evaluate(EvaluationType.REAL, cm);

    answer = eval.toString();
  }
}

isOperator(String button) {
  if (button == 'x' ||
      button == '/' ||
      button == '-' ||
      button == '+' ||
      button == '.') {
    return true;
  }
  return false;
}
