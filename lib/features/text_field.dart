import 'package:flutter/material.dart';
import 'package:flutter_application/features/calculator.dart';

class MyForm extends StatefulWidget {
  const MyForm({super.key});

  @override
  State<MyForm> createState() => _MyFormState();
}

class _MyFormState extends State<MyForm> {
  final myController = TextEditingController();
  String buttonText = 'test';

  @override
  void dispose() {
    myController.dispose();
    super.dispose();
  }

  void _displayValue() {
//    print('print: ${myController.text}');
  }

  @override
  void initState() {
    myController.addListener(_displayValue);
    super.initState();
  }

  String createOrderMessage() {
    var order = fetchUserOrder();
    return 'Your order is: $order';
  }

  Future<String> fetchUserOrder() =>
      Future.delayed(const Duration(seconds: 2), () => 'Order');

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(buttonText),
        SizedBox(
          height: 100,
          width: 200,
          child: TextField(
            controller: myController,
            onChanged: (text) async {
              //    print(createOrderMessage());
            },
          ),
        ),
        InkWell(
          onTap: () async {
            setState(() {
              buttonText = createOrderMessage();
            });
          },
          child: Padding(
            padding: const EdgeInsets.all(0.2),
            child: Container(
              height: 100,
              width: 100,
              color: Colors.blue,
              child: Center(
                  child: Text(
                buttonText,
                style: const TextStyle(
                    color: Colors.black,
                    fontSize: 25,
                    fontWeight: FontWeight.bold),
              )),
            ),
          ),
        ),
        const Image(
          image: AssetImage('assets/images/pompom.png'),
          height: 100,
          width: 100,
        ),
        Center(
          child: TextButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) {
                    return const SecondPage();
                  },
                ),
              );
            },
            child: const Text('Next'),
          ),
        ),
      ],
    );
  }
}
