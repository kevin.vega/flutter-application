import 'package:flutter/material.dart';

class MyButton extends StatelessWidget {
  final Color? color;
  final Color? textColor;
  final String buttonText;
  final Function()? buttonTapped;

  const MyButton(
      {super.key,
      this.color = Colors.white,
      this.textColor = Colors.black,
      required this.buttonText,
      this.buttonTapped});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: buttonTapped,
      child: Padding(
        padding: const EdgeInsets.all(0.2),
        child: Container(
          color: color,
          child: Center(
              child: Text(
            buttonText,
            style: TextStyle(
                color: textColor, fontSize: 25, fontWeight: FontWeight.bold),
          )),
        ),
      ),
    );
  }
}
